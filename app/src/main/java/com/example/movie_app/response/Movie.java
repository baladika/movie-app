package com.example.movie_app.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Movie {
    @SerializedName("results")
    private List<MovieData> movieDataList;

    public List<MovieData> getMovieDataList() {
        return movieDataList;
    }

    public void setMovieDataList(List<MovieData> movieDataList) {
        this.movieDataList = movieDataList;
    }
}
