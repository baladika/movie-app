package com.example.movie_app.base;

public class BaseParam {

    public static final String DB_NAME = "DB_MOVIE";

    public static final String API_KEY = "f25333e16d0e4a47abe8f6808038ab13";
    public static final String MOVIE_ID = "MOVIE_ID";
    public static final String FAV_ID = "FAV_ID";
    public static final String IMG_URL = "https://image.tmdb.org/t/p/original/";

    public static final String SHARED_PREFS = "SHARED_PREFS";
    public static final String BTN_TEXT_FAV = "BTN_TEXT_FAV";
    public static final String FAVORITE = "FAVORITE";

    public static final String ID_MOVIE_FAV = "ID_MOVIE_FAV";


    public static final String ID_MOVIE = "ID_MOVIE";
    public static final String TITLE_MOVIE = "TITLE_MOVIE";
    public static final String DATE_MOVIE = "DATE_MOVIE";
    public static final String VOTE_MOVIE = "VOTE_MOVIE";
    public static final String IMG_MOVIE = "IMG_MOVIE";


    public static final String MOVIE_SIZE = "MOVIE_SIZE";

}
