package com.example.movie_app.base;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.example.movie_app.dao.entity.MyObjectBox;

import io.objectbox.BoxStore;
import io.objectbox.android.BuildConfig;


public class BaseApp extends Application {

    private static BaseApp baseApp;
    private BoxStore boxStore;
    private Context context;

    public static BaseApp get(Context context){
        return BaseApp.get(context.getApplicationContext());
    }

    public BaseApp(){

    }

    @Override
    public void onCreate() {
        super.onCreate();
        baseApp = this;
        context = getApplicationContext();
        boxStore = MyObjectBox.builder().androidContext(this).name(BaseParam.DB_NAME).build();
        if (BuildConfig.DEBUG){
            Log.d("Base app : %s", BoxStore.getVersion());
        }
    }

    public static BaseApp getBaseApp() {
        return baseApp;
    }

    public static void setBaseApp(BaseApp baseApp) {
        BaseApp.baseApp = baseApp;
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }

    public void setBoxStore(BoxStore boxStore) {
        this.boxStore = boxStore;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
