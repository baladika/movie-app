package com.example.movie_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.movie_app.api.ApiService;
import com.example.movie_app.base.BaseParam;
import com.example.movie_app.component.MovieAdapter;
import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.dao.services.MovieDao;
import com.example.movie_app.dao.services.MovieDaoImp;
import com.example.movie_app.di.module.AppModule;
import com.example.movie_app.pages.main.MainInteractor;
import com.example.movie_app.pages.main.MainPresenter;
import com.example.movie_app.pages.main.MainView;
import com.example.movie_app.response.Movie;
import com.example.movie_app.response.MovieData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MainActivity extends AppCompatActivity implements MainView,MovieAdapter.OnCardInfoListener {

    @BindView(R.id.rvMovie)
    RecyclerView recyclerView;

    private List<MovieData> movieEntityList;

    private MovieAdapter adapter;
    private MainPresenter mainPresenter;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainPresenter = new MainInteractor(this);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.show();

        mainPresenter.getMovie();
    }



    private void getAdapter(List<MovieData> movieDataList) {
        RecyclerView recyclerView = findViewById(R.id.rvMovie);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MovieAdapter(movieDataList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pop_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_search:
                SearchView searchView = (SearchView) item.getActionView();
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        adapter.getFilter().filter(newText);
                        return false;
                    }
                });
                return true;
            case R.id.item_fav:
                Intent intent = new Intent(MainActivity.this, FavoriteActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void OnCardInfoListener(Intent intent) {
        String getId = intent.getStringExtra(BaseParam.ID_MOVIE);
        String getTitle = intent.getStringExtra(BaseParam.TITLE_MOVIE);
        String getDate = intent.getStringExtra(BaseParam.DATE_MOVIE);
        String getVote = intent.getStringExtra(BaseParam.VOTE_MOVIE);
        String getImg = intent.getStringExtra(BaseParam.IMG_MOVIE);

        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setTitle(getTitle);
        movieEntity.setVoteAverage(Double.parseDouble(getVote));
        movieEntity.setPosterPath(getImg);
        movieEntity.setReleaseDate(getDate);
        movieEntity.setMovie_id(getId);
        movieEntity.setLike(true);

        mainPresenter.loadMovieId(movieEntity, getId);


    }

    @Override
    public void onSucces() {
        Toasty.warning(getApplicationContext(), "Movie sudah ada di menu favorite ", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onFailed() {
        Toasty.info(getApplicationContext(), "Id belum pernah di input ", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onSave() {
        Toasty.success(getApplicationContext(), "Movie berhasil ditambah ke favorit! ", Toast.LENGTH_SHORT, true).show();
    }

//    @Override
//    public void onLoadMovie() {
//    }

    @Override
    public void onLoad(List<MovieData> movieDataList) {
        progressDialog.dismiss();
        getAdapter(movieDataList);
    }


}