package com.example.movie_app.dao.services;

import com.example.movie_app.base.BaseApp;
import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.dao.entity.MovieEntity_;

import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;

public class MovieDaoImp implements  MovieDao{

    private BoxStore boxStore = BaseApp.getBaseApp().getBoxStore();

    @Override
    public MovieEntity addMovie(MovieEntity movieEntity) {
        Box<MovieEntity> movieBox = boxStore.boxFor(MovieEntity.class);
        movieBox.put(movieEntity);
        return movieEntity;
    }

    @Override
    public MovieEntity deleteMovie(MovieEntity movieEntity) {
        Box<MovieEntity> movieBox = boxStore.boxFor(MovieEntity.class);
        movieBox.remove(movieEntity);
        return movieEntity;
    }

    @Override
    public MovieEntity deleteAllMovie(MovieEntity movieEntity) {
        Box<MovieEntity> movieBox = boxStore.boxFor(MovieEntity.class);
        movieBox.removeAll();
        return movieEntity;
    }

    @Override
    public MovieEntity findMovieId(int id) {
        Box<MovieEntity> movieBox = boxStore.boxFor(MovieEntity.class);
        return movieBox.get(id);
    }

    @Override
    public MovieEntity findMovieQuery(String id) {
        Box<MovieEntity> movieBox = boxStore.boxFor(MovieEntity.class);
        List<MovieEntity> movieEntities = movieBox.query(MovieEntity_.movie_id.equal(id)).build().find();
        if (!movieEntities.isEmpty()){
            return movieEntities.get(0);
        }
        return null;
    }

    @Override
    public List<MovieEntity> load() {
        Box<MovieEntity> movieBox = boxStore.boxFor(MovieEntity.class);
        return movieBox.getAll();
    }
}
