package com.example.movie_app.dao.services;

import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.response.Movie;

import java.util.List;

public interface MovieDao {
    MovieEntity addMovie(MovieEntity movieEntity);
    MovieEntity deleteMovie(MovieEntity movieEntity);
    MovieEntity deleteAllMovie(MovieEntity movieEntity);
    MovieEntity findMovieId(int id);
    MovieEntity findMovieQuery(String id);
    List<MovieEntity> load();
}
