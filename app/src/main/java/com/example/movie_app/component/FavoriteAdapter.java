package com.example.movie_app.component;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.movie_app.DetailActivity;
import com.example.movie_app.R;
import com.example.movie_app.base.BaseParam;
import com.example.movie_app.dao.entity.MovieEntity;

import java.util.ArrayList;
import java.util.List;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyViewHolder> implements Filterable {


    private List<MovieEntity> movieFavList;
    private List<MovieEntity> movieFavListFiltered;
    private Context context;
    private OnCardInfoListener onCardInfoListener;

    public FavoriteAdapter(List<MovieEntity> movieFavList, Context context) {
        this.movieFavList = movieFavList;
        this.movieFavListFiltered = movieFavList;
        this.onCardInfoListener = ((OnCardInfoListener)context);
    }

    public void setMovie(List<MovieEntity> movieFavList) {
        this.movieFavList = movieFavList;
        this.movieFavListFiltered = movieFavList;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            if (charString.isEmpty()){
                movieFavListFiltered = movieFavList;
            }else {
                List<MovieEntity> filteredList = new ArrayList<>();
                for (MovieEntity myMovie : movieFavList){
                    if (myMovie.getTitle().toLowerCase().contains(charString.toLowerCase())){
                        filteredList.add(myMovie);
                    }
                }
                movieFavListFiltered = filteredList;
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = movieFavListFiltered;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            movieFavListFiltered = (List<MovieEntity>) filterResults.values;
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title;
        TextView txt_date;
        TextView txt_vote;
        ImageView coverImg;
        ImageButton popMenu;
        RelativeLayout itemLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            context = itemView.getContext();
            txt_title = itemView.findViewById(R.id.fav_title);
            txt_date = itemView.findViewById(R.id.fav_date);
            txt_vote = itemView.findViewById(R.id.fav_vote);
            coverImg = itemView.findViewById(R.id.fav_img);
            popMenu = itemView.findViewById(R.id.fav_overflow);
            itemLayout = itemView.findViewById(R.id.item_fav_layout);
        }
    }

    @NonNull
    @Override
    public FavoriteAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fav_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteAdapter.MyViewHolder holder, int position) {
        final MovieEntity movieFav = movieFavListFiltered.get(position);
        holder.txt_title.setText(movieFav.getTitle());
        holder.txt_date.setText(movieFav.getReleaseDate());
        holder.txt_vote.setText(String.valueOf(movieFav.getVoteAverage()));


        Glide.with(context)
                .load(movieFav.getPosterPath())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .into(holder.coverImg);

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context.getApplicationContext(), "bool "+movieFav.getLike(), Toast.LENGTH_SHORT).show();
                String id =  String.valueOf(movieFav.getMovie_id());
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra(BaseParam.MOVIE_ID, id);
                intent.putExtra(BaseParam.FAVORITE, movieFav.getLike());
                context.startActivity(intent);
            }
        });

        holder.popMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("Hapus "+movieFav.getTitle()+" dari daftar favorite?");
                builder.setCancelable(true);
                builder.setNegativeButton("Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        Long setId = movieFav.getId();
                        String myId = String.valueOf(setId);
                        intent.putExtra(BaseParam.ID_MOVIE_FAV, myId);

                        onCardInfoListener.OnCardInfoListener(intent);
                    }
                });
                builder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieFavListFiltered.size();
    }

    public interface OnCardInfoListener{
        void OnCardInfoListener(Intent intent);
    }


}
