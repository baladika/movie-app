package com.example.movie_app.component;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.movie_app.DetailActivity;
import com.example.movie_app.base.BaseParam;
import com.example.movie_app.response.Movie;
import com.example.movie_app.response.MovieData;
import com.example.movie_app.R;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> implements Filterable {


    private List<MovieData> movieDataList;
    private List<MovieData> movieDataListFiltered;
    private Context context;
    private OnCardInfoListener onCardInfoListener;
    private boolean isFavorite;

    public MovieAdapter(List<MovieData> movieDataList, Context context) {
        this.movieDataList = movieDataList;
        this.movieDataListFiltered = movieDataList;
        this.onCardInfoListener = ((OnCardInfoListener)context);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            if (charString.isEmpty()){
                movieDataListFiltered = movieDataList;
            }else {
                List<MovieData> filteredList = new ArrayList<>();
                for (MovieData myMovie : movieDataList){
                    if (myMovie.getTitle().toLowerCase().contains(charString.toLowerCase())){
                        filteredList.add(myMovie);
                    }
                }
                movieDataListFiltered = filteredList;
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = movieDataListFiltered;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            movieDataListFiltered = (List<MovieData>) filterResults.values;
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title;
        TextView txt_date;
        TextView txt_vote;
        ImageView coverImg;
        ImageButton popMenu;
        LinearLayout itemLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            context = itemView.getContext();
            txt_title = itemView.findViewById(R.id.movie_title);
            txt_date = itemView.findViewById(R.id.movie_date);
            txt_vote = itemView.findViewById(R.id.movie_vote);
            coverImg = itemView.findViewById(R.id.movie_img);
            popMenu = itemView.findViewById(R.id.movie_overflow);
            itemLayout = itemView.findViewById(R.id.item_movie_layout);
        }
    }

    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MyViewHolder holder, int position) {

        final MovieData movieData = movieDataListFiltered.get(position);
        holder.txt_title.setText(movieData.getTitle());
        holder.txt_date.setText(movieData.getReleaseDate());
        holder.txt_vote.setText(String.valueOf(movieData.getVoteAverage()));


        Glide.with(context)
                .load(BaseParam.IMG_URL+movieData.getPosterPath())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .into(holder.coverImg);

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id =  String.valueOf(movieData.getId());
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra(BaseParam.MOVIE_ID, id);
                intent.putExtra(BaseParam.FAVORITE, false);
                context.startActivity(intent);
            }
        });

        holder.popMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("Tambahkan "+movieData.getTitle()+" ke favorite?");
                builder.setCancelable(true);
                builder.setNegativeButton("Tambah", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        int setId = movieData.getId();
                        String myId = String.valueOf(setId);
                        intent.putExtra(BaseParam.ID_MOVIE, myId);
                        intent.putExtra(BaseParam.TITLE_MOVIE, movieData.getTitle());
                        intent.putExtra(BaseParam.DATE_MOVIE, movieData.getReleaseDate());
                        intent.putExtra(BaseParam.VOTE_MOVIE, String.valueOf(movieData.getVoteAverage()));
                        intent.putExtra(BaseParam.IMG_MOVIE, BaseParam.IMG_URL+movieData.getPosterPath());
                        onCardInfoListener.OnCardInfoListener(intent);
                    }
                });
                builder.setPositiveButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return movieDataListFiltered.size();
    }

    public interface OnCardInfoListener{
        void OnCardInfoListener(Intent intent);
    }


}
