package com.example.movie_app.pages.main;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.movie_app.api.ApiService;
import com.example.movie_app.base.BaseParam;
import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.dao.services.MovieDao;
import com.example.movie_app.dao.services.MovieDaoImp;
import com.example.movie_app.di.module.AppModule;
import com.example.movie_app.response.Movie;
import com.example.movie_app.response.MovieData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainInteractor implements MainPresenter{

    private MainView mainView;
    private List<MovieEntity> movieEntityList = new ArrayList<>();
    private Context context;

    public MainInteractor(MainView mainView){
        this.mainView = mainView;
    }


    @Override
    public void save(MovieEntity movieEntity, String title,String date, Double vote, String img,String id) {
        MovieDao movieDao = new MovieDaoImp();
        movieEntity.setTitle(title);
        movieEntity.setVoteAverage(vote);
        movieEntity.setPosterPath(img);
        movieEntity.setReleaseDate(date);
        movieEntity.setMovie_id(id);
        movieEntity.setLike(true);
        movieDao.addMovie(movieEntity);

        mainView.onSave();

    }

    @Override
    public void delete(MovieEntity movieEntity, String id) {
        MovieDao movieDao = new MovieDaoImp();
        movieEntity = movieDao.findMovieId(Integer.parseInt(id));
        movieDao.deleteMovie(movieEntity);
    }

    @Override
    public void loadMovieId(MovieEntity movieEntity, String id) {
        MovieDao movieDao = new MovieDaoImp();
        MovieEntity movieQuery = movieDao.findMovieQuery(id);

        try{
            movieQuery.getMovie_id();
            mainView.onSucces();
        }catch (Exception e){
            movieDao.addMovie(movieEntity);
            mainView.onSave();
        }

//        MovieDao movieDao = new MovieDaoImp();
//        MovieEntity movieEntity = movieDao.findMovieQuery(id);

//        if (movieEntity.getMovie_id() == null){
//            mainView.onFailed();
////            movieDao.addMovie()
//        }else {
//
//            movieEntity.getMovie_id();
//            mainView.onSucces();
//
//            Log.d("cok ini",movieEntity.getTitle());
        }


    @Override
    public void loadDummy(MovieEntity movieEntity) {
        MovieDao movieDao = new MovieDaoImp();
        MovieEntity entity = movieDao.findMovieQuery(movieEntity.getMovie_id());
        if(entity == null) {
            movieDao.addMovie(movieEntity);
        }

    }


    @Override
    public void getMovie() {
        ApiService service = AppModule.getRetrofit().create(ApiService.class);
        Call<Movie> call = service.getMovie(BaseParam.API_KEY);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                mainView.onLoad(response.body().getMovieDataList());
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });
    }

}
