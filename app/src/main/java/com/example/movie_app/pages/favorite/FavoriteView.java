package com.example.movie_app.pages.favorite;

import com.example.movie_app.dao.entity.MovieEntity;

import java.util.List;

public interface FavoriteView {

    void onDelete();

    void onLoad(List<MovieEntity> movieEntities);
}
