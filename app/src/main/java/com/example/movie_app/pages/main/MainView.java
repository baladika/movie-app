package com.example.movie_app.pages.main;

import androidx.recyclerview.widget.RecyclerView;

import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.response.MovieData;

import java.util.List;

public interface MainView {
    void onSucces();
    void onFailed();
    void onSave();
    void onLoad(List<MovieData> movieDataList);
}
