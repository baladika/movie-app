package com.example.movie_app.pages.favorite;

import com.example.movie_app.dao.entity.MovieEntity;

public interface FavoritePresenter {

    void delete(MovieEntity movieEntity, String id);

    void deleteAll(MovieEntity movieEntity);

    void load();
}
