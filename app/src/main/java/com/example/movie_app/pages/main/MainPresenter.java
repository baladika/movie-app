package com.example.movie_app.pages.main;

import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.response.MovieData;

import java.util.List;

public interface MainPresenter {
    void save(MovieEntity movieEntity, String title,String date, Double vote, String img,String id);
    void delete(MovieEntity movieEntity, String id);
    void loadMovieId(MovieEntity movieEntity, String id);
    void loadDummy(MovieEntity movieEntity);
    void getMovie();
}
