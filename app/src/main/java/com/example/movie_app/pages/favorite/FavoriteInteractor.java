package com.example.movie_app.pages.favorite;

import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.dao.services.MovieDao;
import com.example.movie_app.dao.services.MovieDaoImp;

import java.util.ArrayList;
import java.util.List;

public class FavoriteInteractor implements FavoritePresenter{

    private FavoriteView favoriteView;
    private List<MovieEntity> movieEntityList = new ArrayList<>();

    public FavoriteInteractor(FavoriteView favoriteView){
        this.favoriteView = favoriteView;
    }

    @Override
    public void delete(MovieEntity movieEntity, String id) {
        MovieDao movieDao = new MovieDaoImp();
        movieEntity = movieDao.findMovieId(Integer.parseInt(id));
        movieDao.deleteMovie(movieEntity);
        favoriteView.onDelete();
    }

    @Override
    public void deleteAll(MovieEntity movieEntity) {
        MovieDao movieDao = new MovieDaoImp();
        movieDao.deleteAllMovie(movieEntity);
    }

    @Override
    public void load() {
        MovieDao movieDao = new MovieDaoImp();
        movieEntityList = movieDao.load();
        favoriteView.onLoad(movieEntityList);
    }
}
