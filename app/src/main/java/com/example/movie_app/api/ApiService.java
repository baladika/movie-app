package com.example.movie_app.api;

import com.example.movie_app.response.Movie;
import com.example.movie_app.response.MovieData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET("/3/discover/movie")
    Call<Movie> getMovie(@Query("api_key") String username);

    @GET("/3/movie/{id}")
    Call<MovieData> getMovieDetail(@Path("id") String id,
                                   @Query("api_key") String username);
}
