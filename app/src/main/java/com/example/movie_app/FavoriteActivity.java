package com.example.movie_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.movie_app.base.BaseParam;
import com.example.movie_app.component.FavoriteAdapter;
import com.example.movie_app.dao.entity.MovieEntity;
import com.example.movie_app.pages.favorite.FavoriteInteractor;
import com.example.movie_app.pages.favorite.FavoritePresenter;
import com.example.movie_app.pages.favorite.FavoriteView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class FavoriteActivity extends AppCompatActivity implements FavoriteView, FavoriteAdapter.OnCardInfoListener {

    @BindView(R.id.rvFav)
    RecyclerView recyclerView;

    private FavoriteAdapter adapter;
    private FavoritePresenter favoritePresenter;
    List<MovieEntity> movieList = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(FavoriteActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.show();

        favoritePresenter = new FavoriteInteractor(this);

        getData();
        favoritePresenter.load();

        progressDialog.dismiss();
    }

    private void getData() {
        setAdapter();
    }

    private void setAdapter() {
        RecyclerView recyclerView = findViewById(R.id.rvFav);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new FavoriteAdapter(movieList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pop_search, menu);
        MenuItem item = menu.findItem(R.id.item_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }

    @Override
    public void onDelete() {
        Toasty.error(getApplicationContext(), "Movie Telah Dihapus", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onLoad(List<MovieEntity> movieList) {
        adapter.setMovie(movieList);

//        if (!movieList.isEmpty()){
//            adapter.setMovie(movieList);
//            Toast.makeText(getApplicationContext(), "kondisi 1 size "+movieList.size(), Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(getApplicationContext(), "kondisi 2 size "+movieList.size(), Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    public void OnCardInfoListener(Intent intent) {
        String getId = intent.getStringExtra(BaseParam.ID_MOVIE_FAV);
        MovieEntity movieEntity = new MovieEntity();
        favoritePresenter.delete(movieEntity, getId);
        favoritePresenter.load();
    }
}