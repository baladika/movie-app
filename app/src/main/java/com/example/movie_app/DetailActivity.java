package com.example.movie_app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.movie_app.api.ApiService;
import com.example.movie_app.base.BaseParam;
import com.example.movie_app.di.module.AppModule;
import com.example.movie_app.response.MovieData;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {


    @BindView(R.id.dt_back_img)
    ImageView img_back;

    @BindView(R.id.dt_img)
    ImageView dt_img;

    @BindView(R.id.dt_title)
    TextView dt_title;

    @BindView(R.id.dt_tagline)
    TextView dt_tagline;

    @BindView(R.id.dt_vote)
    TextView dt_vote;

    @BindView(R.id.dt_time)
    TextView dt_time;

    @BindView(R.id.dt_lang)
    TextView dt_lang;

    @BindView(R.id.dt_desc)
    TextView dt_desc;

    @BindView(R.id.dt_date)
    TextView dt_date;

    private ProgressDialog progressDialog;
    private String id_movie;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);



        progressDialog = new ProgressDialog(DetailActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.show();

        Intent intent = getIntent();
        id_movie =intent.getStringExtra(BaseParam.MOVIE_ID);

        setToolbar();
        getDetail();

    }

    private void getDetail() {

        ApiService service = AppModule.getRetrofit().create(ApiService.class);
        Call<MovieData> call = service.getMovieDetail(id_movie, BaseParam.API_KEY);
        call.enqueue(new Callback<MovieData>() {
            @Override
            public void onResponse(Call<MovieData> call, Response<MovieData> response) {
                MovieData movieData = response.body();
                dt_title.setText(movieData.getTitle());
                dt_tagline.setText(movieData.getTagline());
                dt_vote.setText(String.valueOf(movieData.getVoteAverage()));
                dt_time.setText(String.valueOf(movieData.getRuntime()));
                dt_lang.setText(movieData.getOriginalLanguage().toUpperCase());
                dt_desc.setText(movieData.getOverview());
                dt_date.setText(movieData.getReleaseDate());

                showImg(BaseParam.IMG_URL+movieData.getBackdropPath(),img_back);
                showImg(BaseParam.IMG_URL+movieData.getPosterPath(),dt_img);

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MovieData> call, Throwable t) {

            }
        });
    }

    private void showImg(String url, ImageView target) {
        Glide.with(DetailActivity.this)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_launcher_foreground)
                .into(target);
    }


    private void setToolbar() {
//        hide tollbar
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
//      fullscrren
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }


}